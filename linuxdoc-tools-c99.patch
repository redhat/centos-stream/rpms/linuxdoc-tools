From 0771d5d9a49ccd2e4c10ce1fe9bb14b7f69d4081 Mon Sep 17 00:00:00 2001
From: Florian Weimer <fweimer@redhat.com>
Date: Thu, 13 Apr 2023 18:09:27 +0200
Subject: [PATCH] sgmls: Avoid implicit ints/function declarations in configure
Content-type: text/plain

These C features are no longer part of C99, and future compilers
will no longer support them by default, causing the build to fail.

The changes assume that the usual system headers (<stdlib.h>,
<unistd.h>) are present, but that should not be a problem on current
systems.

Submitted upstream: <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1034362>

---
 sgmls-1.1/configure | 60 +++++++++++++++++++++++++++++++--------------
 1 file changed, 42 insertions(+), 18 deletions(-)

diff --git a/sgmls-1.1/configure b/sgmls-1.1/configure
index 5d3e197..e674d24 100755
--- a/sgmls-1.1/configure
+++ b/sgmls-1.1/configure
@@ -110,13 +110,15 @@ cat >doit.c <<\EOF
 
 #include <ctype.h>
 #include <signal.h>
+#include <stdlib.h>
+#include <unistd.h>
 
 static int whoops()
 {
   _exit(1);
 }
 
-main()
+int main(void)
 {
   int c;
 #ifdef isascii
@@ -213,13 +215,15 @@ else
 fi
 
 cat >doit.c <<\EOF
+#include <stdio.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
     remove("foo");
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -231,13 +235,15 @@ else
 fi
 
 cat >doit.c <<\EOF
+#include <unistd.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
     getopt(argc, argv, "v");
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -248,14 +254,16 @@ else
 	off="$off HAVE_GETOPT"
 fi
 
-cat >doit.c <<\EOF
+cat>doit.c <<\EOF
+#include <unistd.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
     access("foo", 4);
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -267,13 +275,15 @@ else
 fi
 
 cat >doit.c <<\EOF
+#include <unistd.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
     vfork();
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -285,6 +295,8 @@ else
 fi
 
 cat >doit.c <<\EOF
+#include <sys/wait.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
@@ -294,7 +306,7 @@ char **argv;
     int status;
     waitpid(-1, &status, 0);
   }
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -307,13 +319,14 @@ fi
 
 cat >doit.c <<\EOF
 #include <string.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
     strerror(0);
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -326,13 +339,14 @@ fi
 
 cat >doit.c <<\EOF
 #include <strings.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
 	bcopy((char *)0, (char *)0, 0);
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -341,13 +355,14 @@ then
 	# Only use BSD_STRINGS if ANSI string functions don't work.
 	cat >doit.c <<\EOF
 #include <string.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
 	memcpy((char *)0, (char *)0, 0);
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -363,13 +378,14 @@ fi
 
 cat >doit.c <<\EOF
 #include <signal.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
 {
   if (argc == 0)
     raise(SIGINT);
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -382,6 +398,7 @@ fi
 
 cat >doit.c <<\EOF
 #include <stdio.h>
+int
 main(argc, argv)
 int argc;
 char **argv;
@@ -391,7 +408,7 @@ char **argv;
     fsetpos(stdin, &pos);
     fgetpos(stdin, &pos);
   }
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -407,6 +424,7 @@ cat >doit.c <<\EOF
 #include <sys/types.h>
 #include <sys/wait.h>
 
+int
 main(argc, argv)
 int argc;
 char **argv;
@@ -423,7 +441,7 @@ char **argv;
     WTERMSIG(status);
     WSTOPSIG(status);
   }
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -437,13 +455,16 @@ fi
 cat >doit.c <<\EOF
 #include <stdio.h>
 #include <signal.h>
+#include <stdlib.h>
+#include <unistd.h>
+#include <string.h>
 
 static int whoops()
 {
   _exit(1);
 }
 
-main()
+int main(void)
 {
   char buf[30];
 #ifdef SIGSEGV
@@ -470,6 +491,7 @@ fi
 cat >doit.c <<\EOF
 #include <nl_types.h>
 
+int
 main(argc, argv)
 int argc;
 char **argv;
@@ -479,7 +501,7 @@ char **argv;
     catgets(d, 1, 1, "default");
     catclose(d);
   }
-  exit(0);
+  return 0;
 }
 EOF
 
@@ -492,9 +514,11 @@ fi
 
 cat >doit.c <<\EOF
 #include <limits.h>
+#include <stdlib.h>
 
 char c = UCHAR_MAX;
 
+int
 main(argc, argv)
 int argc;
 char **argv;
@@ -512,16 +536,16 @@ then
 	char_signed=
 else
 	cat >doit.c <<\EOF
-main()
+int main(void)
 {
   int i;
 
   for (i = 0; i < 512; i++) {
     char c = (char)i;
     if (c < 0)
-	exit(1);
+	return 1;
   }
-  exit(0);
+  return 0;
 }
 EOF
 
-- 
2.39.2

